CountingApp by Adrien Lessard

1 - The following programs must be in the PATH environment variable:
	trafficintelligence.exe
	CountingApp.exe
	sqlite3.exe
	TrackingApp.exe

2 - Use compute-homography.py to generate an homography for each scene. Refer to this site for help:
	https://bitbucket.org/Nicolas/trafficintelligence/wiki/Camera%20Calibration
	
3 - Run generateBlankMasks.py to create blank masks for each scene. Edit them if needed.

4 - Run generateFrames.py to create a file named frame.png for each scene.
	
4 - Run TrafficIntelligence.bat. Usage:
		TrafficIntelligence.bat path-to-videos n-frames-to-analyse-per-video

5 - Run CountingApp.exe to count vehicles. Use CountingApp.exe -h for help

