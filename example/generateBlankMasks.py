import cv2
import numpy as np
import os
import argparse

parser = argparse.ArgumentParser(description='The program generates a blank file mask.png for each scene in each sub-directory', epilog = '', formatter_class=argparse.RawDescriptionHelpFormatter)
args = parser.parse_args()

rootDir = '.'
for dirName, subdirList, fileList in os.walk(rootDir):
    for fname in fileList:
		if fname.endswith(".mp4") or fname.endswith(".avi"):
			if not os.path.isfile(dirName + '\\' + 'mask.png'):
				print(dirName)
				cap = cv2.VideoCapture(dirName + '\\' + fname)
				img = np.zeros((cap.get(4), cap.get(3), 3), np.uint8)
				img[:] = (255, 255, 255)
				cv2.imwrite(dirName + '\\' + 'mask.png', img)