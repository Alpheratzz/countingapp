@echo off

IF NOT EXIST %1 (
    echo directory %1 does not exist
	goto END
)
IF NOT EXIST %1\world.png (
    echo image %1\world.png does not exist
	goto END
)
IF NOT EXIST %1\frame.png (
    echo image %1\frame.png does not exist
	goto END
)
IF NOT EXIST %1\mask.png (
    echo image %1\mask.png does not exist
	goto END
)
IF NOT EXIST %1\homography.txt (
    echo %1\homography.txt does not exist
	goto END
)


if exist %1\formatted-ti.txt del %1\formatted-ti.txt


for %%f in (%1\*.avi %1\*.mp4) do (
	
	if exist %%f.sqlite del %%f.sqlite
	echo.
	echo ************ Processing %%f now ************
	echo.
	trafficintelligence.exe "tracking.cfg" --tf --video-filename %%f --database-filename %%f.sqlite --nframes %2 --homography-filename %1\homography.txt --mask-filename %1\mask.png
	trafficintelligence.exe "tracking.cfg" --gf --video-filename %%f --database-filename %%f.sqlite --homography-filename %1\homography.txt
	
	for %%G in (%%f) do echo File:%%~nxG >> %1\formatted-ti.txt
	sqlite3.exe %%f.sqlite "SELECT o.object_id, p.frame_number, AVG(p.x_coordinate), AVG(p.y_coordinate) FROM positions p INNER JOIN objects_features o ON o.trajectory_id=p.trajectory_id GROUP BY p.frame_number, o.object_id ORDER BY o.object_id, p.frame_number ASC;"  >> %1\formatted-ti.txt
)
echo Done


:END
	