import cv2
import os
import argparse

parser = argparse.ArgumentParser(description='The program generates the file frame.png for each scene in each sub-directory', epilog = '', formatter_class=argparse.RawDescriptionHelpFormatter)
args = parser.parse_args()

rootDir = '.'
for dirName, subdirList, fileList in os.walk(rootDir):
    for fname in fileList:
		if fname.endswith(".mp4") or fname.endswith(".avi"):
			if not os.path.isfile(dirName + '\\' + 'frame.png'):
				print(dirName)
				cap = cv2.VideoCapture(dirName + '\\' + fname)
				ret, frame = cap.read()
				cv2.imwrite(dirName + '\\' + 'frame.png', frame)