if WScript.Arguments.Count < 2 Then
    WScript.Echo "Please specify the source and the destination files. Usage: xlsToCsv <xls/xlsx source file> <csv destination file>"
    Wscript.Quit
End If

csv_format = 6

Set objFSO = CreateObject("Scripting.FileSystemObject")

src_file = objFSO.GetAbsolutePathName(Wscript.Arguments.Item(0))
dest_file = objFSO.GetAbsolutePathName(WScript.Arguments.Item(1))

dim f
Set f = CreateObject("Scripting.FileSystemObject") 
If f.FileExists(dest_file) Then
f.DeleteFile dest_file
End If

Dim oExcel
Set oExcel = CreateObject("Excel.Application")

Dim oBook
Set oBook = oExcel.Workbooks.Open(src_file)
Set wb = oExcel.ActiveWorkbook
Set ws = wb.Worksheets("Totals")

ws.SaveAs dest_file, csv_format

oBook.Close False
oExcel.Quit