#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
__author__ = "Francois Belisle"
__status__ = "Development"
__email__ = "francois.belisle@wspgroup.com"

# dependencies : 
# sqlite3
# pandas 

def batch_format_ti(directory):

     import os
     sub_dirs = [os.path.join(directory,)]
     
     for root, dirs, files in os.walk(directory):
          for dir in dirs:
               path = os.path.join(root, dir)
               sub_dirs.append(path)
     
     print(sub_dirs)
     import multiprocessing as mp
     nb_processes = (mp.cpu_count()*2)-1
     pool = mp.Pool(processes = nb_processes)

     from itertools import repeat
     pool.map_async(format_ti, sub_dirs)

     pool.close()
     pool.join()


def format_ti(directory):
     """
     This function converts sqlite formatted TrafficIntelligence output
     to CountingApp text input
     
     Input : directory
     Side-effect : file conversion to txt.
     """
     import os
     db_regex = os.path.join(directory, "*.sqlite")

     import glob
     dbs = sorted(glob.glob(db_regex))

     if dbs == []:
          return False

     print('Converting : ' + db_regex)

     filename = os.path.join(directory,'formatted-ti.csv')
     if os.path.exists(filename):
          os.remove(filename)

     for database in dbs:
          import sqlite3
          db = sqlite3.connect(database)

          import pandas as pd
          df = pd.read_sql("SELECT \
          o.object_id, p.frame_number, AVG(p.x_coordinate), AVG(p.y_coordinate)\
          FROM positions p \
          INNER JOIN objects_features o \
          ON o.trajectory_id=p.trajectory_id \
          GROUP BY p.frame_number, o.object_id \
          ORDER BY o.object_id, p.frame_number ASC;", db)
          db.close()

          with open(filename, 'a') as f:
               f.write('FILE:'+database+'\n')
               df.to_csv(f, sep='|', index=False, header=False)

     return True

if __name__ == "__main__" :

     import argparse
     parser = argparse.ArgumentParser(
          description='This script formats TrafficIntelligences output for \
          CountingApp input : sqlite -> txt in batch from a results directory')


     parser.add_argument('--results-dir', 
                         dest = 'results_dir',
                         help = 'results directory ',
                         default = 'results')
     
     args = parser.parse_args()

     results_dir = args.results_dir
     
     batch_format_ti(results_dir)

