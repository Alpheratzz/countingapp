#!/bin/bash
# converting all files in a dir to utf8 recursively

if [ "$#" -ne 1 ]; then
	echo "usage: all_to_utf-8.sh <directory>"
	exit 1
fi

shopt -s globstar
for f in ${1}/* ${1}/**/*
do
	if [ -f "$f" ]
	then
		CHARSET="$( file -bi "$f"|awk -F "=" '{print $2}')"
		if [ "$CHARSET" != utf-8 ] && [ "$CHARSET" != binary ]
		then
			echo -e "Converting $f from $CHARSET to utf-8"
			iconv -f "$CHARSET" -t utf8 "$f" > tmpfile
			mv tmpfile "$f"
		fi
	fi
done

echo "Done"
