#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
__author__ = "Francois Belisle"
__status__ = "Development"
__email__ = "francois.belisle@wspgroup.com"

# dependencies : 
# sqlite3

def batch_add_video_info(directory):

     sqlites = get_all(directory, '.sqlite')
     videos = [get_original_video(sqlite) for sqlite in sqlites]
     
     import multiprocessing as mp
     nb_processes = (mp.cpu_count()*2)-1
     pool = mp.Pool(processes = nb_processes)
     pool.map_async(add_video_info, zip(videos, sqlites))

     pool.close()
     pool.join()

def get_all(directory, types):
     files = []
     import os
     for root, directories, filenames in os.walk(directory):
          for filename in filenames:
               if filename.endswith(types):
                    files.append(os.path.join(root,filename))

     return files

def get_original_video(sqlite_filename):
     """
     Idiosyncratic to RD1-00027-00 project 
     """

     video_path = sqlite_filename.replace('Results', 'Videos')
     return video_path.replace('sqlite', 'mp4')

def add_video_info((video_filename, database_filename)):
     """
     This functions adds metadata from video_filename to database_filename
     Specifically a new table video_info with : 
     - total number of frames
     - frames per second
     
     """

     import cv2
     cap = cv2.VideoCapture(video_filename)
     if not cap.isOpened():
          print("File : " + video_filename + " could not be opened")
          
     video_length = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
     #video_fps = cap.get(cv2.cv.CV_CAP_PROP_FPS) does not work, explains this:

     cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, video_length-10)
     millis = cap.get(cv2.cv.CV_CAP_PROP_POS_MSEC)
     video_fps = float(video_length)/(millis/1000) + 0.015

     height = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
     width =  int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))

     import sqlite3
     print(database_filename)
     connexion = sqlite3.connect(database_filename, isolation_level=None)
     db = connexion.cursor()

     db.execute('CREATE TABLE video_info (length INTEGER, fps REAL, height INTEGER, width INTEGER)')
     db.execute('INSERT INTO video_info (length, fps, height, width) VALUES (?,?,?,?)', (video_length, video_fps, height, width))

     connexion.close()

     return True

if __name__ == "__main__" :

     import argparse
     parser = argparse.ArgumentParser(
          description='This script adds a table to TrafficIntelligences sqlite \
                       output for CountingApp input')

     parser.add_argument('--results-dir', 
                         dest = 'results_dir',
                         help = 'results directory ',
                         default = 'results')
     
     args = parser.parse_args()

     results_dir = args.results_dir
     batch_add_video_info(results_dir)

