#include "CountArea.h"
#include "AsyncInput.h"

using namespace std;
using namespace cv;

void mouseHandler(int event, int x, int y, int flags, void* userdata)
{
	CountArea* c = (CountArea*)userdata;
	switch (event)
	{
		case EVENT_LBUTTONDOWN:
			c->userDefinedPoint = Point2f((float)x, (float)y);
			switch (c->selStep)
			{
				case FIRST_POINT:
					c->selStep = SECOND_POINT;
					break;
				case SECOND_POINT:
					c->selStep = AREA_SELECTION;
					break;
				case AREA_SELECTION:
					c->selStep = CONFIRM_SEL;
					break;
				default:
					break;
			}

			break;
		case EVENT_MOUSEMOVE:
			switch (c->selStep)
			{
				case SECOND_POINT:
					c->drawLine(x, y);
					break;
				case AREA_SELECTION:
					c->drawArea(x, y);
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}
}

CountArea::CountArea(Mat* m)
{
	initialMat = m;
	drawingMat = m->clone();
	thickness = 20;
}

void CountArea::drawLine(int x, int y)
{
	drawingMat = initialMat->clone();
	line(drawingMat, p1, Point2f((float)x, (float)y), Scalar(0, 0, 255), 2);
}

int CountArea::calculateDirection(Point2f p)
{
	if (dirVect.x * p.x + dirVect.y * p.y < 0)
		return -1;
	else
		return 1;
}

/* Expects a unit vector */
float CountArea::calculateAngle(Point2f p)
{
	return acos(dirVect.x * p.x + dirVect.y * p.y);
}

void CountArea::drawArea(int x, int y)
{
	drawingMat = initialMat->clone();

	thickness = distPtDte(Point2f((float)x, (float)y), A, B, C);

	float theta;
	if(B == 0)
		theta = PI / 2;
	else
		theta = atan(m);

	Point2f c1, c2, c3, c4;
	c1 = Point2f(p1.x + thickness * cos(theta + PI / 2), p1.y + thickness * sin(theta + PI / 2));
	c2 = Point2f(p2.x + thickness * cos(theta + PI / 2), p2.y + thickness * sin(theta + PI / 2));
	c3 = Point2f(p1.x + thickness * cos(theta - PI / 2), p1.y + thickness * sin(theta - PI / 2));
	c4 = Point2f(p2.x + thickness * cos(theta - PI / 2), p2.y + thickness * sin(theta - PI / 2));

	line(drawingMat, c1, c2, Scalar(0, 0, 255), 2);
	line(drawingMat, c3, c4, Scalar(0, 0, 255), 2);
	line(drawingMat, c1, c3, Scalar(0, 0, 255), 2);
	line(drawingMat, c2, c4, Scalar(0, 0, 255), 2);

	/* Determine which way the traffic is going */
	dirVect = getUnitVector(Point2f(cos(theta + PI / 2), sin(theta + PI / 2)));
	direction = calculateDirection(Point2f((float)x, (float)y) - middle);

	/* UI: draw a line to show the direction */
	line(drawingMat, middle, Point2f(middle.x + direction * 2 * thickness * cos(theta + PI / 2), middle.y + direction * 2 * thickness * sin(theta + PI / 2)), Scalar(0, 0, 0), 8);
	line(drawingMat, middle, Point2f(middle.x + direction * 2 * thickness * cos(theta + PI / 2), middle.y + direction * 2 * thickness * sin(theta + PI / 2)), Scalar(0, 255, 255), 2);
}

void CountArea::selectionProcess(CountingApp& c)
{
	cout << "Define an exclusion area on the scene" << endl;

	string windowName = "CountingApp : " + c.projectName;
	setMouseCallback(windowName, mouseHandler, this);

	selStep = FIRST_POINT;

	while (selStep == FIRST_POINT)
	{
		refreshImage(initialMat, windowName);
	}

	p1 = userDefinedPoint;

	while (selStep == SECOND_POINT)
	{
		refreshImage(&drawingMat, windowName);
	}

	p2 = userDefinedPoint;
	recalculate();

	while (selStep == AREA_SELECTION)
	{
		refreshImage(&drawingMat, windowName);
	}

	p3 = userDefinedPoint;

	setMouseCallback(windowName, NULL, NULL);

	refreshImage(initialMat, windowName);
}

bool CountArea::inCountArea(Point2f p)
{
	/* Obtain the equation of a line drawn by the user */
	float mPerpendicular, APerpendicular, BPerpendicular, CPerpendicular;

	if (B == 0) // case: vertical line
	{
		APerpendicular = 0;
		BPerpendicular = 1;
		CPerpendicular = -middle.y;
	}
	else if (m != 0)
	{
		mPerpendicular = -1 / m;
		float bPerpendicular = middle.y - mPerpendicular * middle.x;
		APerpendicular = - mPerpendicular;
		BPerpendicular = 1;
		CPerpendicular = - bPerpendicular;
	}
	else // case: horizontal line
	{
		APerpendicular = 1;
		BPerpendicular = 0;
		CPerpendicular = -middle.x;
	}

	return (distPtDte(p, A, B, C) < thickness && distPtDte(p, APerpendicular, BPerpendicular, CPerpendicular) < lineLength / 2);
}

void CountArea::recalculate()
{
	middle = middlePoint(p1, p2);
	lineLength = distance2pt(p1, p2);
	if(p1.x == p2.x)
	{
		if(p1.y == p2.y)
		{
			// Points with same coordinates
			A = 0;
			B = 0;
			C = 0;
		}
		// Vertical line
		A = 1;
		B = 0;
		C = -p1.x;
	}
	else
	{
		// Getting the line equation
		m = (p1.y - p2.y) / (p1.x - p2.x);
		float b = p1.y - m * p1.x;

		A = -m;
		B = 1;
		C = -b;
	}
}

void computeLinearRegression(vector<Point2f>& regressionPoints, float& m)
{
	float sumX = 0, sumY = 0, sumXY = 0, sumXX = 0;
	int i, n;

	n = (int)regressionPoints.size();

	for (i = 0; i < n; i++)
	{
		sumX += regressionPoints[i].x;
		sumY += regressionPoints[i].y;
		sumXY += regressionPoints[i].x * regressionPoints[i].y;
		sumXX += regressionPoints[i].x * regressionPoints[i].x;
	}
	
	m = (n * sumXY - sumX * sumY) / (n * sumXX - sumX * sumX);
}

void CountArea::excludeObjects(std::vector<TrackedObj*>& objs, std::vector<TrackPoint*>& points)
{
	for (auto it = points.begin(); it != points.end(); it++)
		if (inCountArea((*it)->center))
			objs[(*it)->objectId]->hidden = true;
}

void CountArea::excludeDirectionalObjects(std::vector<TrackedObj*>& objs, std::vector<TrackPoint*>& points, CountingApp& c)
{
	unsigned int i;
	int* countLineCross;

	countLineCross = (int*)calloc(objs.size(), sizeof(int));
	if (!countLineCross)
		cout << "calloc failed" << endl;

	for (auto it = points.begin(); it != points.end(); it++)
	{
		if (inCountArea((*it)->center))
		{
			vector<Point2f> regressionPoints;

			int nPointsForward = 0;
			TrackPoint* forwardPoint = (*it);
			
			while (forwardPoint->next != nullptr && nPointsForward < c.minPointsTraj - 1)
			{
				forwardPoint = forwardPoint->next;
				regressionPoints.push_back(forwardPoint->center);
				nPointsForward++;
			}

			int nPointsBackward = 0;
			TrackPoint* backwardPoint = (*it);
			while (backwardPoint->prev != nullptr && nPointsBackward < c.minPointsTraj - 1)
			{
				backwardPoint = backwardPoint->prev;
				regressionPoints.push_back(backwardPoint->center);
				nPointsBackward++;
			}

			if (nPointsBackward > 0 && nPointsForward > 0 && nPointsBackward + nPointsForward >= c.minPointsTraj)
			{
				float m;

				computeLinearRegression(regressionPoints, m);
				Point2f dir = getUnitVector(Point2f(forwardPoint->center.x, forwardPoint->center.x * m) - Point2f(backwardPoint->center.x, backwardPoint->center.x * m));

				if (direction == calculateDirection(dir))
				{
					if (abs(calculateAngle(dir) - PI / 2) > PI / 2 - c.maxAngleRad)
					{
						countLineCross[(*it)->objectId]++;
					}
				}
			}
			regressionPoints.clear();
		}
	}

	for (i = 0; i < objs.size(); i++)
		if (countLineCross[i] > 0)
			objs[i]->hidden = true;

	free(countLineCross);
}
