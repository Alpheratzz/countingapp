#include "Path.h"
#include <sstream>
#include <fstream>
#include <iostream>

using namespace std;

int Path::next_id = 0;

Path::Path(int id, string name, int next_id = -1)
{
	this->id = id;
	this->name = name;
	if (next_id != -1)
		this->next_id = next_id;
}

Path* Path::generate(string name)
{
	Path* instance = new Path(next_id, name);
	next_id++;
	return instance;
}

int Path::nextId()
{
	return next_id;
}

void Path::save(std::ofstream& saveFile)
{
	saveFile.write((char*)&id, sizeof(id));

	unsigned int length = name.length() + 1;
	saveFile.write((char*)&length, sizeof(length));
	const char* cname = name.c_str();
	saveFile.write(cname, length);
}

void Path::load(std::ifstream& saveFile)
{
	saveFile.read((char*)&id, sizeof(id));

	unsigned int length;
	saveFile.read((char*)&length, sizeof(length));
	char* cname = new char[length];
	saveFile.read(cname, length);
	name = cname;
	delete[] cname;
}

void Path::save(std::ofstream& saveFile, std::vector<Path*>& data)
{
	size_t s = data.size();
	saveFile.write((char*)&s, sizeof(s));
	for(auto it = data.begin(); it != data.end(); it++)
		(*it)->save(saveFile);

	/* Static members */
	saveFile.write((char*)&next_id, sizeof(next_id));
}

void Path::load(std::ifstream& saveFile, std::vector<Path*>& data)
{
	size_t size;
	saveFile.read((char*)&size, sizeof(size));

	/* Populate structure */
	data.clear();
	for(unsigned i = 0; i < size; i++)
	{
		Path* element = new Path();
		element->load(saveFile);
		data.push_back(element);
	}

	/* Static members */
	saveFile.read((char*)&next_id, sizeof(next_id));
}
