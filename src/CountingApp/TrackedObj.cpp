#include "TrackedObj.h"

using namespace std;
using namespace cv;

TrackedObj::TrackedObj()
{
	firstAppearance = 0;
	hidden = false;
	pathId = 0;
}

void TrackedObj::save(std::ofstream& saveFile)
{
	saveFile.write((char*)&firstAppearance, sizeof(firstAppearance));
	saveFile.write((char*)&id, sizeof(id));
	saveFile.write((char*)&count, sizeof(count));
	saveFile.write((char*)&hidden, sizeof(hidden));
	saveFile.write((char*)&pathId, sizeof(pathId));
}

void TrackedObj::load(std::ifstream& saveFile)
{
	saveFile.read((char*)&firstAppearance, sizeof(firstAppearance));
	saveFile.read((char*)&id, sizeof(id));
	saveFile.read((char*)&count, sizeof(count));
	saveFile.read((char*)&hidden, sizeof(hidden));
	saveFile.read((char*)&pathId, sizeof(pathId));

	color = Scalar(rand() % 256, rand() % 256, rand() % 256);
}

void TrackedObj::save(std::ofstream& saveFile, std::vector<TrackedObj*>& data)
{
	size_t s = data.size();
	saveFile.write((char*)&s, sizeof(s));
	for(auto it = data.begin(); it != data.end(); it++)
		(*it)->save(saveFile);
}

void TrackedObj::load(std::ifstream& saveFile, std::vector<TrackedObj*>& data)
{
	size_t size;
	saveFile.read((char*)&size, sizeof(size));

	/* Populate structure */
	data.clear();
	for(unsigned i = 0; i < size; i++)
	{
		TrackedObj* element = new TrackedObj();
		element->load(saveFile);
		data.push_back(element);
	}
}
