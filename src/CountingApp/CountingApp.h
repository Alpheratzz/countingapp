#ifndef COUNTINGAPP_H
#define COUNTINGAPP_H

#include <opencv2/opencv.hpp>
#include <boost/filesystem.hpp>
#include "CountVideo.h"
#include "TrackedObj.h"
#include "TrackPoint.h"
#include "Path.h"

class CountingApp
{
public:
	CountingApp();
	int save(boost::filesystem::path p);
	int load(boost::filesystem::path p);

	/* Main CountingApp data structures */
	std::vector<CountVideo*> videos;
	std::vector<TrackedObj*> objs;
	std::vector<TrackPoint*> points;
	std::vector<Path*> paths;

	/* Homography */
	cv::Mat homography;

	/* CountingApp options */
	boost::filesystem::path homographyFilename, trackerName, saveFileName, workingDirectory, backgroundImage;
	int vidSectionSec, minPointsTraj;
	unsigned laggControl;
	float maxAngleRad;
	bool quiet, usingSaveFile, tempDisableLaggControl;
	std::string projectName;
	static int version;
	static int subversion;
};

float atoff(const char* str);

unsigned atou(const char* str);

void refreshImage(cv::Mat* m, std::string windowName);

float distPtDte(cv::Point2f p, float A, float B, float C);

cv::Point2f middlePoint(cv::Point2f p1, cv::Point2f p2);

float distance2pt(cv::Point2f p1, cv::Point2f p2);

cv::Point2f getUnitVector(cv::Point2f p);

bool LoadMatrix(const std::string& path, cv::Mat& outMatrix);

boost::filesystem::path mergePaths(boost::filesystem::path base, boost::filesystem::path append);

#endif
