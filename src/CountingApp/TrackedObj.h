#ifndef TRACKEDOBJ_H
#define TRACKEDOBJ_H

#include <opencv2/opencv.hpp>
#include <fstream>
#include <vector>

class TrackedObj
{
public:
	TrackedObj();

	void save(std::ofstream& saveFile);
	void load(std::ifstream& saveFile);

	static void save(std::ofstream& saveFile, std::vector<TrackedObj*>& data);
	static void load(std::ifstream& saveFile, std::vector<TrackedObj*>& data);

	cv::Scalar color;
	int firstAppearance;
	unsigned id;
	int count;
	bool hidden;
	int pathId;
};

#endif
