#ifndef COUNT_VIDEO_H
#define COUNT_VIDEO_H

#include <fstream>
#include <vector>

class CountVideo
{
public:
	CountVideo(){};
	CountVideo(int nFrames, double fps, int height, int width);

	void save(std::ofstream& saveFile);
	void load(std::ifstream& saveFile);

	static void save(std::ofstream& saveFile, std::vector<CountVideo*>& data);
	static void load(std::ifstream& saveFile, std::vector<CountVideo*>& data);

	double fps;
	int nFrames;
	int width;
	int height;
	int frameOffset;
	int objectOffset;

	static CountVideo* lastVideo;
	static unsigned objectOffsetCounter;
	static unsigned frameOffsetCounter;
};

#endif
