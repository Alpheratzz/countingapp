#ifndef TRAJECTORY_H
#define TRAJECTORY_H

#include <string>
#include <fstream>
#include <vector>

class Path
{
public:
	Path(){};
	static Path* generate(std::string name);
	static int nextId();
	int getId(){ return id; }
	std::string getName(){ return name; }

	void save(std::ofstream& saveFile);
	void load(std::ifstream& saveFile);

	static void save(std::ofstream& saveFile, std::vector<Path*>& data);
	static void load(std::ifstream& saveFile, std::vector<Path*>& data);
	
private:
	Path(int id, std::string name, int next_id);
	int id;
	std::string name;
	static int next_id;
};

#endif
