#ifndef TRACKPOINT_H
#define TRACKPOINT_H

#include <opencv2/opencv.hpp>
#include <fstream>
#include <vector>

class TrackPoint
{
public:
	TrackPoint();

	void save(std::ofstream& saveFile);
	void load(std::ifstream& saveFile);

	static void save(std::ofstream& saveFile, std::vector<TrackPoint*>& data);
	static void load(std::ifstream& saveFile, std::vector<TrackPoint*>& data);

	int objectId;
	int frame_number;
	cv::Point2f center;
	TrackPoint* next;
	TrackPoint* prev;
};

#endif