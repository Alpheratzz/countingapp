#include "CountingApp.h"
#include <fstream>

using namespace cv;
using namespace std;
namespace fs = boost::filesystem;

int CountingApp::version = 2;
int CountingApp::subversion = 3;

CountingApp::CountingApp()
{
	srand((unsigned int)time(NULL));
}


int CountingApp::save(boost::filesystem::path p)
{
	ofstream saveFile(mergePaths(workingDirectory, p).string(), ios::binary);
	if(!saveFile.is_open())
		return EXIT_FAILURE;

	saveFile.write((char*)&CountingApp::version, sizeof(CountingApp::version));
	saveFile.write((char*)&CountingApp::subversion, sizeof(CountingApp::subversion));

	Path::save(saveFile, paths);
	TrackedObj::save(saveFile, objs);
	CountVideo::save(saveFile, videos);
	TrackPoint::save(saveFile, points);

	saveFile.close();

	return EXIT_SUCCESS;
}

int CountingApp::load(boost::filesystem::path p)
{
	ifstream saveFile(mergePaths(workingDirectory, p).string(), ios::binary);
	if(!saveFile.is_open())
		return EXIT_FAILURE;

	int saveFileVersion, saveFileSubversion;
	saveFile.read((char*)&saveFileVersion, sizeof(saveFileVersion));
	saveFile.read((char*)&saveFileSubversion, sizeof(saveFileSubversion));
	if(saveFileVersion != CountingApp::version || saveFileSubversion != CountingApp::subversion)
		return EXIT_FAILURE;

	Path::load(saveFile, paths);
	TrackedObj::load(saveFile, objs);
	CountVideo::load(saveFile, videos);
	TrackPoint::load(saveFile, points);

	saveFile.close();

	return EXIT_SUCCESS;
}


float atoff(const char* str)
{
	return (float)atof(str);
}

unsigned atou(const char* str)
{
	return (unsigned)atoi(str);
}

void refreshImage(Mat* m, string windowName)
{
	imshow(windowName, *m);
	waitKey(1);
}

float distPtDte(Point2f p, float A, float B, float C)
{
	return abs(A * p.x + B * p.y + C) / sqrt(pow(A, 2) + pow(B, 2));
}

Point2f middlePoint(Point2f p1, Point2f p2)
{
	Point2f middle;

	middle.x = p1.x + (p2.x - p1.x) / 2;
	middle.y = p1.y + (p2.y - p1.y) / 2;

	return middle;
}

float distance2pt(Point2f p1, Point2f p2)
{
	return sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2));
}

Point2f getUnitVector(Point2f p)
{
	if (p.x != 0 || p.y != 0)
	{
		float divisor = sqrt(p.x * p.x + p.y * p.y);
		p.x /= divisor;
		p.y /= divisor;
		return p;
	}
	return Point2f(0, 0);
}

bool LoadMatrix(const std::string& path, cv::Mat& outMatrix)
{
	string fileline;
	ifstream h(path);
	if (h.is_open())
	{
		int i = 0;
		while (getline(h, fileline))
		{
			/* Read a line */
			istringstream currentLine(fileline);
			string val;

			int j = 0;
			while (getline(currentLine, val, ' '))
			{
				outMatrix.at<float>(i, j) = (float)atof(val.c_str());
				j++;
			}

			i++;
		}
		return true;
	}
	else
	{
		cout << "Unable to load file " << path << ". Using identity instead." << endl;
		return false;
	}
}

fs::path mergePaths(fs::path base, fs::path append)
{
	if (append.is_absolute())
		return append;
	return base / append;
}
