#include "TrackPoint.h"

using namespace std;
using namespace cv;

TrackPoint::TrackPoint()
{
	objectId = 0;
	frame_number = 0;
	center = cv::Point2f(0, 0);
	next = nullptr;
	prev = nullptr;
}

void TrackPoint::save(std::ofstream& saveFile)
{
	saveFile.write((char*)&objectId, sizeof(objectId));
	saveFile.write((char*)&frame_number, sizeof(frame_number));
	saveFile.write((char*)&center.x, sizeof(center.x));
	saveFile.write((char*)&center.y, sizeof(center.y));
}

void TrackPoint::load(std::ifstream& saveFile)
{
	saveFile.read((char*)&objectId, sizeof(objectId));
	saveFile.read((char*)&frame_number, sizeof(frame_number));
	saveFile.read((char*)&center.x, sizeof(center.x));
	saveFile.read((char*)&center.y, sizeof(center.y));
}

void TrackPoint::save(std::ofstream& saveFile, std::vector<TrackPoint*>& data)
{
	size_t s = data.size();
	saveFile.write((char*)&s, sizeof(s));
	for(auto it = data.begin(); it != data.end(); it++)
		(*it)->save(saveFile);
}

void TrackPoint::load(std::ifstream& saveFile, std::vector<TrackPoint*>& data)
{
	size_t size;
	saveFile.read((char*)&size, sizeof(size));

	/* Populate structure */
	data.clear();
	for(unsigned i = 0; i < size; i++)
	{
		TrackPoint* element = new TrackPoint();
		element->load(saveFile);
		data.push_back(element);
	}

	/* Assign chain links */
	for(auto it = data.begin(); it != data.end(); it++)
	{
		if(it != data.begin())
		{
			auto prev = it;
			prev = std::prev(it);
			if((*it)->objectId == (*prev)->objectId)
				(*it)->prev = (*prev);
		}

		if(it != std::prev(data.end()))
		{
			auto next = it;
			next = std::next(it);
			if((*it)->objectId == (*next)->objectId)
				(*it)->next = (*next);
		}
	}
}
