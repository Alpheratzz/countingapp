#include "CountVideo.h"

using namespace std;

unsigned CountVideo::frameOffsetCounter = 0;
unsigned CountVideo::objectOffsetCounter = 0;
CountVideo* CountVideo::lastVideo = nullptr;

CountVideo::CountVideo(int nFrames, double fps, int height, int width)
{
	this->fps = fps;
	this->nFrames = nFrames;
	this->width = width;
	this->height = height;
	this->frameOffset = frameOffsetCounter;
	this->objectOffset = objectOffsetCounter;
	frameOffsetCounter += this->nFrames;
	lastVideo = this;
}

void CountVideo::save(ofstream& saveFile)
{
	saveFile.write((char*)&fps, sizeof(fps));
	saveFile.write((char*)&nFrames, sizeof(nFrames));
	saveFile.write((char*)&width, sizeof(width));
	saveFile.write((char*)&height, sizeof(height));
	saveFile.write((char*)&frameOffset, sizeof(frameOffset));
	saveFile.write((char*)&objectOffset, sizeof(objectOffset));
}

void CountVideo::load(ifstream& saveFile)
{
	saveFile.read((char*)&fps, sizeof(fps));
	saveFile.read((char*)&nFrames, sizeof(nFrames));
	saveFile.read((char*)&width, sizeof(width));
	saveFile.read((char*)&height, sizeof(height));
	saveFile.read((char*)&frameOffset, sizeof(frameOffset));
	saveFile.read((char*)&objectOffset, sizeof(objectOffset));
}

void CountVideo::save(std::ofstream& saveFile, std::vector<CountVideo*>& data)
{
	size_t s = data.size();
	saveFile.write((char*)&s, sizeof(s));
	for(auto it = data.begin(); it != data.end(); it++)
		(*it)->save(saveFile);
}

void CountVideo::load(std::ifstream& saveFile, std::vector<CountVideo*>& data)
{
	size_t size;
	saveFile.read((char*)&size, sizeof(size));

	/* Populate structure */
	data.clear();
	for(unsigned i = 0; i < size; i++)
	{
		CountVideo* element = new CountVideo();
		element->load(saveFile);
		data.push_back(element);
	}
}
