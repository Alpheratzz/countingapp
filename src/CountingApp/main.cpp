#include <opencv2/opencv.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "TrackedObj.h"
#include "TrackPoint.h"
#include "CountingApp.h"
#include "CountArea.h"
#include "Path.h"
#include "AsyncInput.h"
#include "Logger.h"
#include "TrafficIntelligenceLoader.h"

using namespace cv;
using namespace std;
namespace po = boost::program_options;
namespace fs = boost::filesystem;

/* Variables from command-line arguments */
CountingApp c;

void drawTrackedPoints(Mat& m);
int commandLineParser(int argc, char* argv[], CountingApp& c);
void showPoints(int pathId);
void printActionList();
void printPaths(vector<Path*> p);

int main(int argc, char* argv[])
{
	if (commandLineParser(argc, argv, c) == EXIT_FAILURE)
		return EXIT_FAILURE;

	/* Acquire homography */
	cv::Mat identity = cv::Mat(3, 3, CV_32FC1);
	for (unsigned i = 0; i < 3; i++)
		for (unsigned j = 0; j < 3; j++)
			if (j != i)
				identity.at<float>(i, j) = 0;
			else
				identity.at<float>(i, j) = 1;
	
	c.homography = cv::Mat(3, 3, CV_32FC1);
	if (!LoadMatrix(mergePaths(c.workingDirectory, c.homographyFilename).string(), c.homography))
		c.homography = identity;
	c.homography = c.homography.inv();


	/* Load objects either with a save file or from the tracker's output */
	if (c.usingSaveFile)
	{
		if(c.load(mergePaths(c.workingDirectory, c.saveFileName)) == EXIT_FAILURE)
		{
			cout << "Could not open save file " << mergePaths(c.workingDirectory, c.saveFileName) << endl;
			return EXIT_FAILURE;
		}
	}
	else
	{
		/* Acquire SQL data */
		TrafficIntelligenceLoader ti = TrafficIntelligenceLoader(&c);
		if(ti.load() == EXIT_FAILURE)
		{
			cout << "Error while loading tracker data" << endl;
			return EXIT_FAILURE;
		}
		c.paths.push_back(Path::generate("Untracked"));
		c.paths.push_back(Path::generate("Rejected"));
	}

	/* Matrix for UI display */
	Mat initial_mat = imread(c.backgroundImage.string());
	string windowName = "CountingApp : " + c.projectName;
	cv::namedWindow(windowName ,1);
	Mat m = initial_mat.clone();
	
	int undoIndex = -1;
	bool save = true;
	int workingPath = 0;

	showPoints(workingPath);

	while (true)
	{
		/* Print application state and possible commands */
		printActionList();
		printPaths(c.paths);

		/* Save state */
		if(save)
		{
			undoIndex++;
			fs::path p = mergePaths(c.workingDirectory, ".undo" + to_string(undoIndex) + ".cap");
			c.save(p);
			save = false;
		}

		/* Draw all tracked points */
		m = initial_mat.clone();
		drawTrackedPoints(m);
		rectangle(m, Point2f(0,0), Point2f(200, 20), Scalar(0, 0, 0), CV_FILLED);
		putText(m, c.paths[workingPath]->getName(), Point2f(5,15), FONT_HERSHEY_PLAIN, 1,  Scalar(255, 255, 255));
		refreshImage(&m, windowName);
		char key = (char)cv::waitKey();

		if(key >= '0' && key <= '9')
		{
			if(key - '0' < (int)c.paths.size())
			{
				workingPath = key - '0';
				showPoints(workingPath);
			}
		}
		if(key == 'p') // path
		{
			cout << "Enter a path id: ";
			unsigned pathId = cvCompatibleInput<unsigned>();
			if(pathId < c.paths.size())
			{
				workingPath = pathId;
				showPoints(workingPath);
			}
		}
		if (key == 'd') // directional
		{
			CountArea ca = CountArea(&m);
			ca.selectionProcess(c);
			ca.excludeDirectionalObjects(c.objs, c.points, c);
			save = true;
		}
		if (key == 'a') // all
		{
			CountArea ca = CountArea(&m);
			ca.selectionProcess(c);
			ca.excludeObjects(c.objs, c.points);
			save = true;
		}
		if (key == 'q') // quit process
			break;
		if (key == 'n') // add to new Path
		{
			int pathId = Path::nextId();
			cout << "Displayed objects will be associated with a new path (Id = " << pathId << ") named: " << endl;
			c.paths.push_back(Path::generate(cvCompatibleInput<string>()));
			for (auto it = c.points.begin(); it != c.points.end(); it++)
				if(!c.objs[(*it)->objectId]->hidden)
					c.objs[(*it)->objectId]->pathId = pathId;
			showPoints(workingPath);
			save = true;
		}
		if (key == 'x') // permanent delete
		{
			for (auto it = c.points.begin(); it != c.points.end(); it++)
				if(!c.objs[(*it)->objectId]->hidden)
					c.objs[(*it)->objectId]->pathId = 1;
			showPoints(workingPath);
			save = true;
		}
		if (key == 'm') // merge
		{
			cout << "Merge with path (id = ) " << endl;
			int id = cvCompatibleInput<int>();
			for (auto it = c.points.begin(); it != c.points.end(); it++)
				if(!c.objs[(*it)->objectId]->hidden)
					c.objs[(*it)->objectId]->pathId = id;
			showPoints(workingPath);
			save = true;
		}
		if (key == 'l') // hide
		{
			c.tempDisableLaggControl = !c.tempDisableLaggControl;
			if (c.tempDisableLaggControl)
				cout << "Lagg control: enabled" << endl;
			else
				cout << "Lagg control: disabled" << endl;
		}
		if (key == 'h') // hide
		{
			for (auto it = c.points.begin(); it != c.points.end(); it++)
				c.objs[(*it)->objectId]->hidden = true;
			save = true;
		}
		if (key == 'g') // generate report
		{
			cout << "Report file name (csv): " << endl;
			fs::path name(cvCompatibleInput<string>());
			double framesTotal = c.videos.back()->frameOffset + c.videos.back()->nFrames;
			double secsTotal = (double)framesTotal / c.videos[0]->fps;
			int n_vid_sections = (int)ceil(secsTotal / c.vidSectionSec);
			int n_traj = (int)c.paths.size();

			// allocate dynamic array
			int **countData = new int*[n_vid_sections];
			for (int i = 0; i < n_vid_sections; ++i)
				countData[i] = new int[n_traj];
			for (int i = 0; i < n_vid_sections; i++)
				for (int j = 0; j < n_traj; j++)
					countData[i][j] = 0;
			
			for (auto o = c.objs.begin(); o != c.objs.end(); o++)
				for (auto t = c.paths.begin(); t != c.paths.end(); t++)
					if ((*o)->pathId == (*t)->getId())
						countData[(*o)->firstAppearance / int(c.videos[0]->fps * c.vidSectionSec)][(*t)->getId()]++;

			// Start writing the report on the file
			Logger output(cout, c, name);
			for (auto t = c.paths.begin(); t != c.paths.end(); t++)
				output << (*t)->getName() << ",";
			output << '\n';
			
			for(int i = 0; i < n_vid_sections; i++)
			{
				for(int j = 0; j < n_traj; j++)
					output << countData[i][j] << ",";
				output << '\n';
			}
			
			output.close();
			
			// deallocate dynamic array
			for (int i = 0; i < n_vid_sections; ++i)
				delete countData[i];
			
			delete countData;
		}
		if (key == 's') // save
		{
			cout << "Save file name: " << endl;
			fs::path name(cvCompatibleInput<string>());

			if(c.save(name) == EXIT_FAILURE)
				cout << "Could not save " << name << endl;
			else
				cout << "Saved " << name << endl;
		}

		/* Undo feature */
		if(key == 'z')
		{
			if(undoIndex > 0)
			{
				undoIndex--;
				fs::path p = mergePaths(c.workingDirectory, ".undo" + to_string(undoIndex) + ".cap");
				c.load(p);
			}
		}
	}

	return EXIT_SUCCESS;
}

void drawTrackedPoints(Mat& m)
{
	for (auto it = c.points.begin(); it != c.points.end(); it++)
		if (c.tempDisableLaggControl || (rand() % c.points.size()) < c.laggControl)
			if(!c.objs[(*it)->objectId]->hidden)
				circle(m, (*it)->center, 2, c.objs[(*it)->objectId]->color, -1);
}

int commandLineParser(int argc, char* argv[], CountingApp& c)
{
	po::options_description desc("CountingApp v" + to_string(CountingApp::version) + "." + to_string(CountingApp::subversion));
	desc.add_options()
		("workingDirectory,d", po::value<fs::path>(&c.workingDirectory)->default_value(fs::current_path()), "Default directory for input and output files. CountingApp will search in this directory for .sqlite files. Any absolute path provided will not take workingDirectory into account.")
		("backgroundImage,b", po::value<fs::path>(&c.backgroundImage)->default_value(fs::path("frame.png")), "Background image (frame) over which the trajectories will be displayed")
		("trackerName,r", po::value<fs::path>(&c.trackerName)->default_value(fs::path("tracker.txt")), "Tracker name")
		("homographyFilename,f", po::value<fs::path>(&c.homographyFilename)->default_value(fs::path("homography.txt")), "Name of the homography file")
		("maxAngleRad,m", po::value<float>(&c.maxAngleRad)->default_value(PI / 5), "Max angle between the vehicle's velocity vector and the box's direction vector")
		("vidSectionSec,s", po::value<int>(&c.vidSectionSec)->default_value(900 /* 15 minutes */), "Time of a video difision to be shown in the output file")
		("minPointsTraj,p", po::value<int>(&c.minPointsTraj)->default_value(5), "Minimum number of consecutive track points to consider a trajectory")
		("load,l", po::value<fs::path>(&c.saveFileName), "Name of the save file to load")
		("projectName,n", po::value<string>(&c.projectName)->default_value("Project"), "Name of the project")
		("laggControl,c", po::value<unsigned>(&c.laggControl)->default_value(100000), "Maximum number of points to be displayed at the same time to avoid laggs in enormous files. If set to zero, lagg control is disabled")
		("verbose,v", "Displays only mandatory information on the console") // TODO: to implement
		("help,h", "Displays this help")
		;
	po::variables_map argMap;
	try {
		po::store(po::parse_command_line(argc, argv, desc), argMap);
	}
	catch (boost::exception &) {
		cout << "Invalid option" << endl;
		cout << desc << endl;
		return EXIT_FAILURE;
	}
	po::notify(argMap);

	/* Display help */
	if (argMap.count("help")) {
		cout << desc << endl;
		return EXIT_FAILURE;
	}

	/* Save files */
	c.usingSaveFile = (bool)argMap.count("load");

	/* Booleans */
	c.quiet = argMap.count("verbose") == 0;
	c.tempDisableLaggControl = true;

	return EXIT_SUCCESS;
}

void showPoints(int pathId)
{
	for (auto it = c.objs.begin(); it != c.objs.end(); it++)
		(*it)->hidden = (*it)->pathId != pathId;
}

void printActionList()
{
	cout << endl <<
		"************************" << endl <<
		"* COUNTING APP OPTIONS *" << endl <<
		"************************" << endl <<
		"d - add a directional exclusion area" << endl <<
		"a - add an omnidirectional exclusion area " << endl <<
		"q - quit program" << endl <<
		"n - add currently displayed objects to a new path" << endl <<
		"m - add currently displayed objects to an existing path" << endl <<
		"x - permanently delete currently displayed objects" << endl <<
		"g - generate report" << endl <<
		"h - hide all trajectories" << endl <<
		"r - remove all exclusion areas - displays all objects not belonging to a path yet" << endl <<
		"s - save current state" << endl;
}

void printPaths(vector<Path*> p)
{
	if (Path::nextId() > 0)
	{
		cout << endl <<
			"************************" << endl <<
			"*         PATHS        *" << endl <<
			"************************" << endl;
		for (auto it = p.begin(); it != p.end(); it++)
			cout << (*it)->getId() << "\t" << (*it)->getName() << endl;
		cout << endl;
	}
}
