#ifndef ASYNC_INPUT_H
#define ASYNC_INPUT_H

#include <thread>

template <typename T>
void asyncGet(T* input, volatile bool* done);

template<>
inline void asyncGet(std::string* input, volatile bool* done)
{
	std::string str;
	std::getline(std::cin, str);
	*input = str;
	*done = true;
}

template <typename T>
void asyncGet(T* input, volatile bool* done)
{
	fflush(stdin);
	std::string i;
	getline(std::cin, i);
	std::stringstream ss(i);
	ss >> *input;
	*done = true;
}

template <typename T>
class AsyncInput
{
public:
	AsyncInput()
	{
		done = false;
		std::thread t(asyncGet<T>, &input, &done);
		t.detach();
	}
	bool ready()
	{
		return done;
	}
	T get()
	{
		return input;
	}

private:
	T input;
	volatile bool done;
};

template <typename T>
T cvCompatibleInput()
{
	AsyncInput<T> ai = AsyncInput<T>();
	while (!ai.ready())
	{
		cv::waitKey(1);
	}
	return ai.get();
}

#endif
