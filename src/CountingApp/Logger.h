#ifndef LOHHER_H
#define LOGGER_H

#include <iostream>
#include <fstream>
#include "CountingApp.h"

class Logger
{
public:
	Logger(std::ostream& os1, CountingApp& c, boost::filesystem::path name);
	void close();

	template<class T>
	Logger& operator<<(const T& x);

private:
	std::ostream& os1;
	std::ofstream os2;
	bool quiet;
};

template<class T>
Logger& Logger::operator<<(const T& x) {
	if (!quiet)
		os1 << x;
	os2 << x;
	return *this;
}

#endif