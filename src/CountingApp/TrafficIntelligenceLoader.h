#ifndef TRAFFIC_INTELLIGENCE_LOADER_H
#define TRAFFIC_INTELLIGENCE_LOADER_H

#include "CountingApp.h"
#include "TrackPoint.h"

class TrafficIntelligenceLoader
{
public:
	TrafficIntelligenceLoader(CountingApp* c);
	int load();

	CountingApp* c;

private:
	int loadVideosAndObjects(std::vector<boost::filesystem::path>& databases);
	int loadPoints(std::vector<boost::filesystem::path>& databases);

	friend int pointsCallback(void *data, int argc, char **argv, char **azColName);
	friend int objsCallback(void *data, int argc, char **argv, char **azColName);
	friend int videoCallback(void *data, int argc, char **argv, char **azColName);
	friend void pointsThread(TrafficIntelligenceLoader* ti, std::string path, int i);
	
	std::vector<std::vector<TrackPoint*>> populatePoints;

	const std::string fastLoaderName = "ti_ludicrous_speed_loader.cap";
};

int pointsCallback(void *data, int argc, char **argv, char **azColName);
int objsCallback(void *data, int argc, char **argv, char **azColName);
int videoCallback(void *data, int argc, char **argv, char **azColName);
void pointsThread(TrafficIntelligenceLoader* ti, std::string path, int i);

cv::Point2f project(const cv::Point2f& p, const cv::Mat& homography);

#endif
