#ifndef COUNTAREA_H
#define COUNTAREA_H

#include <fstream>
#include <opencv2/opencv.hpp>
#include "TrackedObj.h"
#include "TrackPoint.h"
#include "CountingApp.h"
#include "CountVideo.h"

#define PI							(3.141592653589793f)

typedef enum
{
	FIRST_POINT,
	SECOND_POINT,
	AREA_SELECTION,
	CONFIRM_SEL
} SELECTION_STEP;

class CountArea
{
public:
	CountArea(cv::Mat* m);

	void drawLine(int x, int y);
	void drawArea(int x, int y);
	void selectionProcess(CountingApp& c);
	bool inCountArea(cv::Point2f p);
	void excludeObjects(std::vector<TrackedObj*>& objs, std::vector<TrackPoint*>& points);
	void excludeDirectionalObjects(std::vector<TrackedObj*>& objs, std::vector<TrackPoint*>& points, CountingApp& c);

	SELECTION_STEP selStep;
	cv::Point2f userDefinedPoint;

private:
	void recalculate();
	int calculateDirection(cv::Point2f p);
	float calculateAngle(cv::Point2f p);

	float m;
	float A, B, C; // line parameters in equation Ax + By + C = 0
	cv::Point2f p1, p2, p3, middle;
	float lineLength;
	float thickness;
	cv::Mat drawingMat;
	cv::Mat* initialMat;
	cv::Point2f dirVect;
	int direction;
};

#endif
