#include "TrafficIntelligenceLoader.h"
#include "CountingApp.h"
#include "external/sqlite/include/sqlite3.h"
#include "Semaphore.h"
#include <thread>

using namespace cv;
using namespace std;
namespace fs = boost::filesystem;

struct pointsCallbackData
{
	TrafficIntelligenceLoader* loader;
	int index;
};


TrafficIntelligenceLoader::TrafficIntelligenceLoader(CountingApp* c)
{
	this->c = c;
}

int TrafficIntelligenceLoader::load()
{
	if(c->load(fastLoaderName) == EXIT_SUCCESS)
		return EXIT_SUCCESS;

	/* Clear temporary data */
	for(auto it = populatePoints.begin(); it != populatePoints.end(); it++)
		(*it).clear();
	populatePoints.clear();

	/* Acquire all databases */
	vector<fs::path> databases;
	fs::directory_iterator end_iter;
	for (fs::directory_iterator dir_itr(c->workingDirectory); dir_itr != end_iter; ++dir_itr)
		if (dir_itr->path().extension() == ".sqlite")
			databases.push_back(dir_itr->path());
	
	/* Sort them */
	sort(databases.begin(), databases.end());
	if (databases.size() == 0)
		return EXIT_FAILURE;

	time_t start = time(NULL);

	if(loadVideosAndObjects(databases) == EXIT_FAILURE)
		return EXIT_FAILURE;

	if(loadPoints(databases) == EXIT_FAILURE)
		return EXIT_FAILURE;

	c->save(fastLoaderName);

	time_t end = time(NULL);
	cout << "Duration: " << end - start << " seconds" << endl;

	return EXIT_SUCCESS;
}

int TrafficIntelligenceLoader::loadVideosAndObjects(vector<fs::path>& databases)
{
	/* Load videos and objects */
	for (auto it = databases.begin(); it != databases.end(); it++)
	{
		/* Open database */
		sqlite3 *db;
		int rc;
		char* zErrMsg = 0;
		rc = sqlite3_open_v2((*it).string().c_str(), &db, SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, NULL);
		if (rc) {
			cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
			sqlite3_close(db);
			return EXIT_FAILURE;
		}

		const char* vidInfoQuery = "SELECT * FROM video_info";
		rc = sqlite3_exec(db, vidInfoQuery, videoCallback, this, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			return EXIT_FAILURE;
		}

		/* Populate objects */
		const char* objsQuery = "SELECT object_id FROM objects_features ORDER BY object_id ASC";
		rc = sqlite3_exec(db, objsQuery, objsCallback, this, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			return EXIT_FAILURE;
		}

		sqlite3_close(db);

		CountVideo::objectOffsetCounter = c->objs.back()->id + 1;
	}
	cout << "Loaded " << c->objs.size() << " objects" << endl;

	return EXIT_SUCCESS;
}

Semaphore* sems;

int TrafficIntelligenceLoader::loadPoints(vector<fs::path>& databases)
{	
	/* Spawn the threads */
	int threadIndex = 0;
	
	int nThreads = thread::hardware_concurrency() + 1;
	sems = new Semaphore(nThreads);

	for(auto it = databases.begin(); it != databases.end(); it++)
		populatePoints.push_back(vector<TrackPoint*>());

	for(auto it = databases.begin(); it != databases.end(); it++)
	{
		sems->wait();
		string path = (*it).string();
		cout << "Processing database " << threadIndex << endl;
		thread(pointsThread, this, path, threadIndex).detach();
		threadIndex++;
	}

	for(int i = 0; i < nThreads; i++)
		sems->wait();

	/* Merge data */
	for (auto it = populatePoints.begin(); it != populatePoints.end(); it++)
		c->points.insert(c->points.end(), (*it).begin(), (*it).end());

	
	cout << "Loaded " << c->points.size() << " points" << endl;

	return EXIT_SUCCESS;
}

int pointsCallback(void *data, int argc, char **argv, char **azColName)
{
	pointsCallbackData* d = (pointsCallbackData*)data;

	TrafficIntelligenceLoader* ti = d->loader;
	int structIndex = d->index;

	CountingApp* c = ti->c;

	unsigned id = atou(argv[0]);
	int frame = atoi(argv[1]);
	float x = atoff(argv[2]);
	float y = atoff(argv[3]);

	/* New point */
	TrackPoint* p = new TrackPoint();
	p->objectId = c->videos[structIndex]->objectOffset + id;
	c->objs[p->objectId]->firstAppearance = frame + c->videos[structIndex]->frameOffset;
	p->frame_number = frame + c->videos[structIndex]->frameOffset;
	p->center = project(Point2f(x, y), c->homography);

	if (ti->populatePoints[structIndex].size() > 0)
	{
		/* Assign chain relation if points are consecutive in the path */
		if (p->objectId == ti->populatePoints[structIndex].back()->objectId)
		{
			p->prev = ti->populatePoints[structIndex].back();
			ti->populatePoints[structIndex].back()->next = p;
		}
	}

	ti->populatePoints[structIndex].push_back(p);

	return 0;
}


int objsCallback(void *data, int argc, char **argv, char **azColName)
{
	TrafficIntelligenceLoader* ti = (TrafficIntelligenceLoader*)data;
	CountingApp* c = ti->c;

	unsigned id = atou(argv[0]);

	if(c->objs.size() == 0 || c->objs.back()->id - CountVideo::lastVideo->objectOffset != id)
	{
		TrackedObj* o = new TrackedObj();
		o->color = Scalar(rand() % 256, rand() % 256, rand() % 256);
		o->id = CountVideo::lastVideo->objectOffset + id;
		o->count = 0;
		c->objs.push_back(o);
	}
	
	return 0;
}

int videoCallback(void *data, int argc, char **argv, char **azColName)
{
	TrafficIntelligenceLoader* ti = (TrafficIntelligenceLoader*)data;
	CountingApp* c = ti->c;
	c->videos.push_back(new CountVideo(atoi(argv[0]), atof(argv[1]), atoi(argv[2]), atoi(argv[3])));
	return 0;
}

void pointsThread(TrafficIntelligenceLoader* ti, string path, int i)
{
	sqlite3 *db;
	int rc;
	char* zErrMsg = 0;
	rc = sqlite3_open_v2(path.c_str(), &db, SQLITE_OPEN_READONLY, NULL);
	if (rc) {
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
	}

	pointsCallbackData data = { ti, i };

	/* Populate videos */
	sqlite3_exec(db, "PRAGMA cache_size = 1000000", NULL, NULL, &zErrMsg);

	/* Get video data */
	char vidDataQuery[] = "SELECT o.object_id, p.frame_number, AVG(p.x_coordinate), AVG(p.y_coordinate) FROM positions p INNER JOIN objects_features o ON o.trajectory_id=p.trajectory_id GROUP BY p.frame_number, o.object_id ORDER BY o.object_id, p.frame_number ASC";
	rc = sqlite3_exec(db, vidDataQuery, pointsCallback, &data, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		sqlite3_free(zErrMsg);
	}

	sqlite3_close(db);
	sems->notify();
}

Point2f project(const Point2f& p, const Mat& homography)
{
	float x, y;
	float w = homography.at<float>(2, 0)*p.x + homography.at<float>(2, 1)*p.y + homography.at<float>(2, 2);
	if (w != 0)
	{
		x = (homography.at<float>(0, 0)*p.x + homography.at<float>(0, 1)*p.y + homography.at<float>(0, 2)) / w;
		y = (homography.at<float>(1, 0)*p.x + homography.at<float>(1, 1)*p.y + homography.at<float>(1, 2)) / w;
	}
	else
	{
		x = 0;
		y = 0;
	}
	return Point2f(x, y);
}
