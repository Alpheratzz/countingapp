#include "Logger.h"

using namespace std;

Logger::Logger(ostream& os1, CountingApp& c, boost::filesystem::path name) : os1(os1)
{
	os2.open(mergePaths(c.workingDirectory,  name).string(), ios::trunc);
	quiet = c.quiet;
}

void Logger::close()
{
	os2.close();
}